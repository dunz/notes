# 쉘 명령어

## 파일내용 클립보드에 복사하기
```sh
pbcopy < [filename]
```

## 파일내용 파일에 복사하기
```sh
cat .zshrc > test.txt
```